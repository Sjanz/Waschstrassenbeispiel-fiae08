package com.Itbh.Fiae08.Waschstrasse;

import java.util.ArrayList;

public class Waschprogramm {
    private String name;
    private ArrayList<Reinigungsschritt> reinigungsschritte = new ArrayList<Reinigungsschritt>();

    public Waschprogramm(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void reinigungsschrittHinzufuegen(Reinigungsschritt neuerReinigungsschritt){
        this.reinigungsschritte.add(neuerReinigungsschritt);
    }

    public void ausfuehren() {
        System.out.println(String.format("Waschprogramm %s läuft...", getName()));

        for (Reinigungsschritt reinigungsschritt : reinigungsschritte) {
            reinigungsschritt.ausfuehren();
        }

        System.out.println(String.format("Waschprogramm %s abgeschlossen...", getName()));
        System.out.println("Auto fährt aus...");
    }
}
