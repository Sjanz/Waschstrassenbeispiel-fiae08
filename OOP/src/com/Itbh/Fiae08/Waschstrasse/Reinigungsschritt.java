package com.Itbh.Fiae08.Waschstrasse;

public class Reinigungsschritt {
    private String name;

    public Reinigungsschritt(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void ausfuehren() {
        System.out.println(String.format("Schwer beschäftigt mit %s!", getName()));

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
