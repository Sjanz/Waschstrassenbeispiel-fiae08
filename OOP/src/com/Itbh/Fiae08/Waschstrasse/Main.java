package com.Itbh.Fiae08.Waschstrasse;

public class Main {
    // Wir sollen eine Software für eine *Waschstraße* schreiben, die folgendes kann:
    // * Soll erkennen können um was für einen *Autotyp* es sich handelt (SUV, Kleinwagen, Transporter),
    // um ein passendes Waschprogramm auszuwählen
    // * Soll eine Reihe von *Waschprogrammen* ausführen können
    // * Ein *Waschprogramm* soll eine Reihe von *Reinigungsschritten* ausführen

    //Klassen: Waschstraße, Autotyp, Waschprogramm, Reinigungsschritt

    //Methoden Waschstraße: erkenneAutotyp(), waschprogrammAusführen(), waschprogrammAuswählen()
    //Methoden Waschprogram: reinigungsschrittAusführen()

    public static void main(String[] args) {
        Waschstrasse meineWaschstrasse = new Waschstrasse();

        Auto bmw = new Auto();

        meineWaschstrasse.erkenneAutotyp(bmw);
        meineWaschstrasse.waschprogrammAuswaehlen();
        meineWaschstrasse.waschprogrammAusfuehren();

    }
}
