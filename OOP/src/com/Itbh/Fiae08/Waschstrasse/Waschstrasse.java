package com.Itbh.Fiae08.Waschstrasse;

public class Waschstrasse {
    private String aktuellerAutotyp;
    private Waschprogramm aktuellesWaschprogramm;

    public void erkenneAutotyp(Auto zuWaschendesAuto) {
        if (zuWaschendesAuto.getTyp().equals("Kleinwagen")) {
            aktuellerAutotyp = zuWaschendesAuto.getTyp();
        }
    }

    public void waschprogrammAuswaehlen() {
        if (this.aktuellerAutotyp.equals("Kleinwagen")) {
            Waschprogramm basiswaesche = new Waschprogramm("Basiswäsche");

            Reinigungsschritt hochdruckVorwaesche = new Reinigungsschritt("Hochdruck-Vorwäsche");
            Reinigungsschritt waschen = new Reinigungsschritt("Waschen");
            Reinigungsschritt trocknen = new Reinigungsschritt("Trocknen");

            basiswaesche.reinigungsschrittHinzufuegen(hochdruckVorwaesche);
            basiswaesche.reinigungsschrittHinzufuegen(waschen);
            basiswaesche.reinigungsschrittHinzufuegen(trocknen);

            this.aktuellesWaschprogramm = basiswaesche;
        }
    }

    public void waschprogrammAusfuehren(){
        this.aktuellesWaschprogramm.ausfuehren();
    }
}
